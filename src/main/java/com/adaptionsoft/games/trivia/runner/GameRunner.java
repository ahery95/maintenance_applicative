
package com.adaptionsoft.games.trivia.runner;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.uglytrivia.Player;


public class GameRunner {

	private static boolean notAWinner;

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("R pour rock et T pour techno");
        String s = in.nextLine();
        System.out.println("Voulez initializer vous-même le nombre de gold ?");
        String r = in.nextLine();

		System.out.println("Entrez le nombre de golds nécessaires à la victoire d'un joueur :");
		int amountOfGoldToWin = 0;

		if ( !r.equals("Non")) {
			do {

				System.out.println("Il faut un minimum de 6 golds");
				amountOfGoldToWin = Integer.parseInt(in.nextLine());

			} while (amountOfGoldToWin < 6);

		} else {
			amountOfGoldToWin = 6;
		}

		Game aGame = new Game(s, amountOfGoldToWin);

		Player playerA = new Player("Joueur 1");
		Player playerB = new Player("Joueur 2");
		Player playerC = new Player("Joueur 3");
		Player playerD = new Player("Joueur 4");
		Player playerE = new Player("Joueur 5");
		Player playerF = new Player("Joueur 6");
		Player playerG = new Player("Joueur 7");
		
		aGame.add(playerA);
		aGame.add(playerB);
		aGame.add(playerC);
		aGame.add(playerD);
		aGame.add(playerE);
		aGame.add(playerF);
		//aGame.add(playerG);
		
		if(aGame.isPlayable()) {
			
			Random rand = new Random();
			
			do {
				
				aGame.roll(rand.nextInt(5) + 1);
				if(aGame.getPlayers().get(aGame.getCurrentPlayer()).isJokerDisponible()){
					System.out.println(aGame.getPlayers().get(aGame.getCurrentPlayer()).getName() + ", voulez vous utiliser votre Joker ? Oui/Non");
					String jokerUtilise = in.nextLine();
					if(jokerUtilise.equals("Oui")) {
						aGame.usedJoker(aGame.getPlayers().get(aGame.getCurrentPlayer()));
						
					} else if(jokerUtilise.equals("Non")) {
						if (rand.nextInt(9) == 7) {
							notAWinner = aGame.wrongAnswer();
						} else {
							notAWinner = aGame.wasCorrectlyAnswered();
						}
					}
					
				} else {
					if (1==1) {
						notAWinner = aGame.wrongAnswer();
					} 
					
					else {
						notAWinner = aGame.wasCorrectlyAnswered();
					}
				}

				if(aGame.getCurrentPlayer() == 0)
				{
					ArrayList<Player> toDelete = new ArrayList<>();
					for(Player onePlayer : aGame.getPlayers()) {
						System.out.println("Voulez vous partir "+ onePlayer.getName() + " ? \n Y pour partir, N pour rester");
						String s2 = in.nextLine();
						if(s2.equals("Y")) {
							toDelete.add(onePlayer);
						}
					}
					for (Player onePlayer : toDelete) {
						aGame.delete(onePlayer);
					}
					aGame.setCurrentPlayer(0);
					if(aGame.howManyPlayers() == 1) {
						Player winner = aGame.getPlayers().get(0);
						System.out.println(winner.getName() + "a gagner par défaut");
						notAWinner = false;
					}
					if(aGame.howManyPlayers() == 0) {
						System.out.println("Match nul");
						notAWinner = false;
					}
				}
			} while (notAWinner);
		} else {
			System.out.println("Le nombre de joueur n'est pas correct");
		}
		
	}
}
