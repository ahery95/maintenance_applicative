package com.adaptionsoft.games.uglytrivia;

public class Player {
	
	private String name;
	
	private boolean jokerDisponible;

	private int timePrison;
	public Player(String name) {
		this.name = name;
		this.jokerDisponible = true;
		this.timePrison = 0;
	}

	public int getTimePrison() {
		return timePrison;
	}

	public void setTimePrison(int timePrison) {
		this.timePrison = timePrison;
	}

	public String getName() {
		return this.name;
	}
	
	public void setString(String newName) {
		this.name = newName;
	}
	
	public boolean isJokerDisponible() {
		return this.jokerDisponible;
	}
	
	public void setIsJokerDisponible(boolean isJokerDisponible) {
		this.jokerDisponible = isJokerDisponible;
	}

}
